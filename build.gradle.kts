// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    dependencies {
        classpath("io.realm:realm-gradle-plugin:7.0.0")
    }
}

plugins {
    id("com.android.application") version "7.3.1" apply false
}