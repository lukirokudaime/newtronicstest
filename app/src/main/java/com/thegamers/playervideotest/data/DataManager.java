package com.thegamers.playervideotest.data;


import com.thegamers.playervideotest.data.api.RestService;
import com.thegamers.playervideotest.data.entity.Response;

import java.util.Map;

import io.reactivex.Observable;

public class DataManager {

    private RestService restService;

    public DataManager(RestService restService) {

        this.restService = restService;

    }

    public Observable<Response> getListVideo() {
        return restService.getListVideo();
    }


}
