package com.thegamers.playervideotest.data.api;

import android.content.Context;

import androidx.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thegamers.playervideotest.BuildConfig;
import com.thegamers.playervideotest.config.Constant;
import com.thegamers.playervideotest.data.api.interceptor.RestHeaderModifierInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestServiceFactory {

    public static RestService create(String token, Context context) {
        OkHttpClient client = makeClientService(makeLoggingInterceptor(), token, context);
        return makeRestService(client, makeGson());
    }

    private static RestService makeRestService(OkHttpClient okHttp, Gson gson) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_REST)
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(RestService.class);
    }

    @NonNull
    private static OkHttpClient makeClientService(
            HttpLoggingInterceptor loggingInterceptor, String token, Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .addInterceptor(new RestHeaderModifierInterceptor(token ,context))
                .addInterceptor(loggingInterceptor)
                /*.sslSocketFactory(CertificateUtil.getUnsafeSSLSocketFactory(), CertificateUtil.getTrustManager())
                .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)*/
                .build();
    }

    @NonNull
    private static HttpLoggingInterceptor makeLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                        : HttpLoggingInterceptor.Level.NONE);
    }

    @NonNull
    private static Gson makeGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }
}
