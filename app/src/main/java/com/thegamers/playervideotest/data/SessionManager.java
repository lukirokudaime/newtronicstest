package com.thegamers.playervideotest.data;

import android.content.SharedPreferences;

public class SessionManager {

    private static final String IS_LOGIN                = "is_login";
    private static final String USER_ID                 = "user_id";
    private static final String ROLE_ID                 = "role_id";
    private static final String USER_NAME               = "user_name";
    private static final String USER_NIK                = "user_nik";
    private static final String USER_SID                = "user_sid";
    private static final String USER_EMAIL              = "user_email";
    private static final String USER_TELEPHONE          = "user_telephone";
    private static final String USER_IMAGE              = "user_image";
    private static final String USER_TOKEN              = "user_token";
    private static final String NOTIF_TOKEN             = "notif_token";
    private static final String JWT_TOKEN               = "jwt_token";
    private static final String USER_MENU_ROLE          = "user_menu_role";


    private final SharedPreferences preferences;

    public SessionManager(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public Boolean isUserLogin() {
        return preferences.getBoolean(IS_LOGIN, false);
    }

    public void setUserLogin() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_LOGIN, true);
        editor.apply();
    }

    public Integer getUserId() {
        return preferences.getInt(USER_ID, 0);
    }

    public void setUserId(Integer userID) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(USER_ID, userID);
        editor.apply();
    }

    public Integer getRoleId() {
        return preferences.getInt(ROLE_ID, 0);
    }

    public void setRoleId(Integer userID) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ROLE_ID, userID);
        editor.apply();
    }

    public String getUserName() {
        return preferences.getString(USER_NAME, "");
    }

    public void setUserName(String userName) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_NAME, userName);
        editor.apply();
    }

    public String getUserNik() {
        return preferences.getString(USER_NIK, "");
    }

    public void setUserNik(String userNik) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_NIK, userNik);
        editor.apply();
    }

    public String getUserSid() {
        return preferences.getString(USER_SID, "");
    }

    public void setUserSid(String userSid) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_SID, userSid);
        editor.apply();
    }

    public String getUserEmail() {
        return preferences.getString(USER_EMAIL, "");
    }

    public void setUserEmail(String userEmail) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_EMAIL, userEmail);
        editor.apply();
    }

    public String getUserTelephone() {
        return preferences.getString(USER_TELEPHONE, "");
    }

    public void setUserTelephone(String userTelephone) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_TELEPHONE, userTelephone);
        editor.apply();
    }

    public String getImageProfile() {
        return preferences.getString(USER_IMAGE, "");
    }

    public void setImageProfile(String imageProfile) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_IMAGE, imageProfile);
        editor.apply();
    }


    public String getUserToken() {
        return preferences.getString(USER_TOKEN, "");
    }

    public void setUserToken(String userToken) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_TOKEN, userToken);
        editor.apply();
    }

    public String getNotifToken() {
        return preferences.getString(NOTIF_TOKEN, "");
    }

    public void setNotifToken(String notifToken) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(NOTIF_TOKEN, notifToken);
        editor.apply();
    }

    public String getJwtToken() {
        return preferences.getString(JWT_TOKEN, "");
    }

    public void setJwtToken(String jwtToken) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(JWT_TOKEN, jwtToken);
        editor.apply();
    }

    public void clearData() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(IS_LOGIN);
        editor.remove(USER_ID);
        editor.remove(USER_NAME);
        editor.remove(USER_NIK);
        editor.remove(USER_SID);
        editor.remove(USER_EMAIL);
        editor.remove(USER_TELEPHONE);
        editor.remove(USER_TOKEN);
        editor.remove(NOTIF_TOKEN);
        editor.remove(JWT_TOKEN);
        editor.remove(ROLE_ID);

        editor.apply();
    }
}
