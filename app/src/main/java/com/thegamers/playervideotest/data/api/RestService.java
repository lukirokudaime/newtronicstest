package com.thegamers.playervideotest.data.api;


import com.thegamers.playervideotest.config.Constant;
import com.thegamers.playervideotest.data.entity.Response;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface RestService {

    @GET(Constant.END_POINT + "/directory/dataList")
    Observable<Response> getListVideo();

}
