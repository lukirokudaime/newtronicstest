package com.thegamers.playervideotest.data.entity;

import com.google.gson.annotations.SerializedName;

public class PlaylistItem{

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private Integer id;

	@SerializedName("dir_id")
	private Integer dirId;

	@SerializedName("title")
	private String title;

	@SerializedName("type")
	private String type;

	@SerializedName("url")
	private String url;

	private Boolean isDownload = false;

	private String video_offline;

	public String getVideo_offline() {
		return video_offline;
	}

	public void setVideo_offline(String video_offline) {
		this.video_offline = video_offline;
	}

	public Boolean getDownload() {
		return isDownload;
	}

	public void setDownload(Boolean download) {
		isDownload = download;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return id;
	}

	public void setDirId(Integer dirId){
		this.dirId = dirId;
	}

	public Integer getDirId(){
		return dirId;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}
}