package com.thegamers.playervideotest.data.api.interceptor;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RestHeaderModifierInterceptor implements Interceptor {

    private String token;
    private Context context;

    public RestHeaderModifierInterceptor(String token, Context context){
        this.token = token;
        this.context = context;
    }

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request()
                .newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", token)
                .build();
        Response response = chain.proceed(request);

        /*if(!response.isSuccessful()){
            APIError error = new APIError(response.code(),response.message(),response.header("refNumber"));
            Timber.d("DEVELOPER : %s", error);
            if(error.getRefNumber() != null)
                backgroundThreadToast(context,"Error Rest API Service with Ref Number: "+error.getRefNumber());
            else
                if (error.getStatusCode() == 401){
                    backgroundThreadToast(context,""+error.getStatusCode());
                }else {
                    backgroundThreadToast(context,"Error Occurred with Response: "+error.getStatusCode()+" "+error.getMessage());
                }

        }*/

        return response;
    }

    public static void backgroundThreadToast(final Context context, final String msg){
        if(context != null && msg != null){
            new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(context, msg, Toast.LENGTH_LONG).show());
        }
    }
}
