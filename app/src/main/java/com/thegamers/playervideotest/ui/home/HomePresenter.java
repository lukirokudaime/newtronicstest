package com.thegamers.playervideotest.ui.home;


import com.thegamers.playervideotest.base.BasePresenter;
import com.thegamers.playervideotest.data.DataManager;

import io.reactivex.Scheduler;

public class HomePresenter extends BasePresenter<HomeView> {

    private DataManager manager;
    private Scheduler androidScheduler;
    private Scheduler processScheduler;

    public HomePresenter(DataManager manager, Scheduler androidScheduler, Scheduler processScheduler) {
        this.manager = manager;
        this.androidScheduler = androidScheduler;
        this.processScheduler = processScheduler;
    }

    public void getVideoList() {
        disposable.add(manager.getListVideo()
                .observeOn(androidScheduler).subscribeOn(processScheduler)
                .subscribe(list -> {
                    if (isViewAttached()) {
                        getView().showList(list);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(throwable);
                    }
                }));
    }


}
