package com.thegamers.playervideotest.ui.home;

import com.thegamers.playervideotest.base.BaseView;
import com.thegamers.playervideotest.data.entity.Response;

public interface HomeView extends BaseView {
    void showList(Response dataResponse);
}
