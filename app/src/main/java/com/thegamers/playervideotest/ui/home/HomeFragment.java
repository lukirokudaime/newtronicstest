package com.thegamers.playervideotest.ui.home;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.squareup.picasso.Picasso;
import com.thegamers.playervideotest.adapter.AdapterButtonFilter;
import com.thegamers.playervideotest.adapter.AdapterListPlayer;
import com.thegamers.playervideotest.base.BaseFragment;
import com.thegamers.playervideotest.data.entity.PlaylistItem;
import com.thegamers.playervideotest.data.entity.Response;
import com.thegamers.playervideotest.databinding.FragmentHomeBinding;
import com.thegamers.playervideotest.helper.DownloadUtilLibrary;
import com.thegamers.playervideotest.helper.WebViewInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment implements AdapterButtonFilter.ClickListener, AdapterListPlayer.onChoicheMovie, AdapterListPlayer.onDownload, WebViewInterface, AdapterListPlayer.onDeleted, HomeView {

    private FragmentHomeBinding binding;
    private AdapterButtonFilter adapterButton;
    private AdapterListPlayer adapterList;
    private HomeViewModel homeViewModel;
    private List<String> itemList;
    private List<PlaylistItem> playerList;
    private PlaylistItem moviePlayer;
    private Integer posisi;

    SimpleExoPlayerView exoPlayerView;

    // creating a variable for exoplayer
    SimpleExoPlayer exoPlayer;

    HomePresenter presenter;
    private AlertDialog loadingDialog;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        exoPlayerView = binding.idExoPlayerVIew;

        presenter = new HomePresenter(getManager(), getAndroidScheduler(), getProcessScheduler());
        presenter.attachView(this);

        initView();
        loadingData();


        return root;
    }

    private void loadingData() {
        startLoadingDialog(loadingDialog);
        presenter.getVideoList();
    }

    private void initView() {
        itemList = new ArrayList<>();
        playerList = new ArrayList<>();
        homeViewModel.getButton().observe(getViewLifecycleOwner(),itemList::addAll);

        adapterButton = new AdapterButtonFilter(this, requireContext(),itemList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvButton.setLayoutManager(layoutManager);
        binding.rvButton.setAdapter(adapterButton);
        adapterButton.notifyDataSetChanged();

        adapterList = new AdapterListPlayer(this, requireContext(), playerList, this, this);
        LinearLayoutManager lyt = new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvMovie.setLayoutManager(lyt);
        binding.rvMovie.setAdapter(adapterList);

        loadingDialog = getLoadingDialog(binding.idExoPlayerVIew);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onEdit(String item) {

    }

    @Override
    public void onChoice(PlaylistItem player, Integer position) {
        binding.tvDeskripsi.setText(player.getDescription());
        binding.tvTitle.setText(player.getTitle());
        moviePlayer = player;
        posisi = position;
        if (player.getDownload()){
            Uri myUri = Uri.parse(moviePlayer.getUrl());
            String mime = requireContext().getContentResolver().getType(myUri);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(myUri, mime);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        }else {
            if (player.getType().equals("video")){
                binding.idExoPlayerVIew.setVisibility(View.VISIBLE);
                binding.thumbnail.setVisibility(View.GONE);
                playVideo(player.getUrl());
            }else {
                binding.idExoPlayerVIew.setVisibility(View.GONE);
                Toast.makeText(requireContext(), "URL Video Kosong", Toast.LENGTH_SHORT).show();
                Picasso.get().load(moviePlayer.getUrl()).into(binding.thumbnail);
            }

        }

    }

    public void deleteFromExternalStorage(String fileName) {
        File dir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS);
        File folder = new File(dir + "/NewtronicsMovie");

        String fullPath = folder.getAbsolutePath();
        try
        {
            File file = new File(fullPath, fileName);
            if(file.exists())
                file.delete();
            playerList.remove(moviePlayer);

            PlaylistItem player = new PlaylistItem();
            player.setCreatedAt(moviePlayer.getCreatedAt());
            player.setDescription(moviePlayer.getDescription());
            player.setId(moviePlayer.getId());
            player.setTitle(moviePlayer.getTitle());
            player.setType(moviePlayer.getType());
            player.setDirId(moviePlayer.getDirId());
            player.setUrl(moviePlayer.getUrl());
            player.setUpdatedAt(moviePlayer.getUpdatedAt());
            player.setDownload(false);
            player.setVideo_offline("");

            playerList.add(posisi, player);
            adapterList.notifyDataSetChanged();
            Toast.makeText(requireContext(), "Berhasil Hapus File", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e)
        {
            Toast.makeText(requireContext(), "Gagal Hapus File", Toast.LENGTH_SHORT).show();
        }
    }

    private void playVideo(String movieUrl) {
        try {

            // BandwidthMeter is used for
            // getting default bandwidth
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

            // track selector is used to navigate between
            // video using a default seekbar.
            TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));

            // we are adding our track selector to exoplayer.
            exoPlayer = ExoPlayerFactory.newSimpleInstance(requireContext(), trackSelector);

            // we are parsing a video url
            // and parsing its video uri.
            Uri videouri = Uri.parse(movieUrl);

            // we are creating a variable for datasource factory
            // and setting its user agent as 'exoplayer_view'
            DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");

            // we are creating a variable for extractor factory
            // and setting it to default extractor factory.
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

            // we are creating a media source with above variables
            // and passing our event handler as null,
            MediaSource mediaSource = new ExtractorMediaSource(videouri, dataSourceFactory, extractorsFactory, null, null);

            // inside our exoplayer view
            // we are setting our player
            exoPlayerView.setPlayer(exoPlayer);

            // we are preparing our exoplayer
            // with media source.
            exoPlayer.prepare(mediaSource);

            // we are setting our exoplayer
            // when it is ready.
            exoPlayer.setPlayWhenReady(true);

        } catch (Exception e) {
            // below line is used for
            // handling our errors.
            Log.e("TAG", "Error : " + e.toString());
        }
    }

    @Override
    public void downloaded(PlaylistItem player, Integer position) {
        moviePlayer = player;
        posisi = position;
        if (player.getType().equals("video")){
            DownloadUtilLibrary downloadUtil = new DownloadUtilLibrary(requireContext(), this, player.getTitle());
            downloadUtil.execute(player.getUrl());
        }else {
            showToast("Video tidak ada..");
        }
    }

    @Override
    public void onDoneDownloadFile(File file, String status) {
        if (status.equals("sukses")) {
            Uri uri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(requireContext(), requireContext().getPackageName()+".fileprovider", file);
            }
            else {
                uri = Uri.fromFile(file);
            }
            playerList.remove(moviePlayer);
            PlaylistItem player = new PlaylistItem();

            player.setCreatedAt(moviePlayer.getCreatedAt());
            player.setDescription(moviePlayer.getDescription());
            player.setId(moviePlayer.getId());
            player.setTitle(moviePlayer.getTitle());
            player.setType(moviePlayer.getType());
            player.setDirId(moviePlayer.getDirId());
            player.setUrl(moviePlayer.getUrl());
            player.setUpdatedAt(moviePlayer.getUpdatedAt());
            player.setDownload(true);
            player.setVideo_offline(String.valueOf(uri));

            playerList.add(posisi, player);
            adapterList.notifyDataSetChanged();


            try {
                String mime = requireContext().getContentResolver().getType(uri);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, mime);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(requireContext(), "Saved in "+file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else {
            Toast.makeText(requireContext(), "Gagal Download", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void deletedVideoDownload(PlaylistItem player, Integer position) {
        moviePlayer = player;
        deleteFromExternalStorage(player.getTitle()+".mp4");
    }

    @Override
    public void showError(Throwable throwable) {
        stopLoadingDialog(loadingDialog);
        showToast(throwable.getLocalizedMessage());
    }

    @Override
    public void showList(Response dataResponse) {
        stopLoadingDialog(loadingDialog);
        playerList.addAll(dataResponse.getData().get(0).getPlaylist());
        adapterList.notifyDataSetChanged();
    }
}