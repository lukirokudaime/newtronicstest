package com.thegamers.playervideotest.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import java.util.ArrayList;
import java.util.List;

public class HomeViewModel extends ViewModel {
    private final MutableLiveData<List<String>> buttonsList;

    public HomeViewModel() {
        buttonsList = new MutableLiveData<>();

        List<String> list = new ArrayList<>();
        list.add("Produk");
        list.add("Materi");
        list.add("Layanan");
        list.add("Example");

        buttonsList.setValue(list);

    }
    public LiveData<List<String>> getButton(){
        return buttonsList;
    }
}