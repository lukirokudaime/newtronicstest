package com.thegamers.playervideotest.config;

import android.Manifest;


public class Constant {

    public static final Boolean DEVELOPER_MODE     = false;

    /*__________________________________==DEVELOPMENT STAGING==____________________________________________*/

    public static final String JWT_TOKEN         = "EMPTY";

    private static final String URL_DEMO           = "http://27.112.79.143";

    public static final String END_POINT     = "/api";

    public static final String URL_REST           = URL_DEMO;


    /*__________________________________ACCESS_PERMISSION_________________________________________________*/

    public static final String[] PERMISSIONS_HSE_APP = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

}