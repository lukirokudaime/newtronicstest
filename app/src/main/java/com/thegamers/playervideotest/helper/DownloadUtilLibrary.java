package com.thegamers.playervideotest.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import timber.log.Timber;

public class DownloadUtilLibrary extends AsyncTask<String, String, String> {
    private ProgressDialog progressDialog;
    private File fileDownload;
    private Context context;
    private WebViewInterface webViewInterface;
    private String type = "";
    private String fileName = "";

    public DownloadUtilLibrary(Context context, WebViewInterface webViewInterface, String fileName) {
        this.context = context;
        this.webViewInterface = webViewInterface;
        this.fileName = fileName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * Before starting background thread
     * Show Progress Bar Dialog
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setTitle("Downloading Please Wait...");
        this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();
    }

    /**
     * Downloading file in background thread
     */
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            URL url = new URL(f_url[0]);
            URLConnection connection = url.openConnection();
            connection.connect();

            String mimeType = ".mp4";
            String name_of_file = fileName+mimeType;
            /*String disposition = connection.getHeaderField("Content-Disposition");

            if (disposition!=null){
                int index = disposition.indexOf("filename=");
                if (index>0){
                    fileName = disposition.substring(index+10, disposition.length()-1);
                }
            }
            else {
                fileName = FileUploadUtil.getFileName(f_url[0]);
            }*/

            int lengthOfFile = connection.getContentLength();
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

//            String fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1);
//            String[] finalFileName = fileName.split("\\?");
//            String fileName = FileUploadUtil.getFileName(f_url[0]);

            if (getType().equals("resource")){
                File cacheDir = context.getExternalCacheDir();
                String rootDir = cacheDir.getAbsolutePath() + "/.filePdf";
                File root = new File(rootDir);

                if(!root.exists())
                    root.mkdirs();
                fileDownload = new File(root, name_of_file);
            }
            else {
                //Creating Path
                File dir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOWNLOADS);
                File folder = new File(dir + "/NewtronicsMovie");

                if (!folder.exists()) {
                    //Create Folder From Path
                    folder.mkdir();
                }

                //Path And Filename.type
                fileDownload = new File(folder, name_of_file);

//                fileDownload = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
            }

            OutputStream output = new FileOutputStream(fileDownload);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress("" + (int) ((total * 100) / lengthOfFile));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
            return "sukses";

        } catch (Exception e) {
            Timber.e("Error: %s", e.getMessage());
        }

        return "gagal";
    }

    /**
     * Updating progress bar
     */
    protected void onProgressUpdate(String... progress) {
        progressDialog.setProgress(Integer.parseInt(progress[0]));
    }


    @Override
    protected void onPostExecute(String message) {
        this.progressDialog.dismiss();
        webViewInterface.onDoneDownloadFile(fileDownload, message);
    }
}
