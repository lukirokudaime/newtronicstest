package com.thegamers.playervideotest.helper;

import android.content.Context;
import android.content.SharedPreferences;
public class Helper {

    private static final String PREF_NAME = "test_pref_name";

    public static SharedPreferences getDefaultPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, 0);
    }
}
