package com.thegamers.playervideotest.helper;

import android.Manifest;

import java.util.HashMap;
import java.util.Map;

public class Constant {

    public static final Boolean DEVELOPER_MODE     = false;

    /*__________________________________==DEVELOPMENT DEVELOPMENT==____________________________________________*/

    // Temporary update for dev user with SECURITY
    public static final String JWT_TOKEN         = "";
    private static final String URL_DEMO           = "";
    public static final String ENDPOINT     = "";

    public static final String URL_REST           = URL_DEMO;
    public static final String URL_REST_SIGNAL   = "";
    public static final String URL_PHOTO_BY_PATH = URL_DEMO + ENDPOINT + "??";


    public static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static final String[] REQUIRED_SDK_PERMISSIONS_13 = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_MEDIA_VIDEO,
            Manifest.permission.READ_MEDIA_AUDIO
    };
}