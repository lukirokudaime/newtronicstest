package com.thegamers.playervideotest.helper;

import java.io.File;

public interface WebViewInterface {
    void onDoneDownloadFile(File file, String status);
}
