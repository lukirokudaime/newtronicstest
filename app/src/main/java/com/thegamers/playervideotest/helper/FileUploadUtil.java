package com.thegamers.playervideotest.helper;

import android.util.Base64;
import android.util.Base64OutputStream;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FileUploadUtil {
    @NonNull
    public static RequestBody createPartString(String string) {
        return RequestBody.create(
                MultipartBody.FORM, string);
    }

    @NonNull
    public static MultipartBody.Part createPartFile(String partName, String filePath) {
        File file = new File(filePath);

        RequestBody requestFile =
                RequestBody.create(
                        MultipartBody.FORM,
                        file
                );

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public static String getBase64File(String path) {
        InputStream inputStream;
        String encodedFile= "", lastVal;
        try {
            inputStream = new FileInputStream(path);

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile =  output.toString();
        }
        catch (FileNotFoundException e1 ) {
            e1.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;
    }

    public static String getMimeType(String eks) {
        String type = null;
        if (eks != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(eks);
        }
        return type;
    }

    public static String getExtensionType(String url) {
        return MimeTypeMap.getFileExtensionFromUrl(url);
    }

    public static String getFileName(String url) {
        return URLUtil.guessFileName(url, null, getMimeType(getExtensionType(url)));
    }
}
