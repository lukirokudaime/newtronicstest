package com.thegamers.playervideotest.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavOptions;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.thegamers.playervideotest.R;
import com.thegamers.playervideotest.data.DataManager;
import com.thegamers.playervideotest.data.SessionManager;
import com.thegamers.playervideotest.data.api.RestService;
import com.thegamers.playervideotest.data.api.RestServiceFactory;
import com.thegamers.playervideotest.helper.Helper;

import org.jetbrains.annotations.NotNull;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseFragment extends Fragment {

    private RestService restService;
    private DataManager manager;
    private SessionManager sessionManager;

    protected Context context;
    protected Activity activity;

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (Activity) context;
    }

    public RestService getRestService() {
        if (restService == null) {
            restService = RestServiceFactory.create(getJwtToken(),context);
        }
        return restService;
    }


    public DataManager getManager() {
        if (manager == null) {
            manager = new DataManager(getRestService());
        }
        return manager;
    }

    public SessionManager getSessionManager() {
        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
        }
        return sessionManager;
    }


    protected void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    protected NavOptions getNavOptions() {
        return new NavOptions.Builder()
                .setPopEnterAnim(R.anim.slide_in_left)
                .setPopExitAnim(R.anim.slide_out_right)
                .setEnterAnim(R.anim.slide_in_right)
                .setExitAnim(R.anim.slide_out_left)
                .build();
    }

    protected void startLoadingDialog(AlertDialog loadingDialog) {

        if (loadingDialog == null) {
            return;
        }

        if (loadingDialog.isShowing()) {
            loadingDialog.setCancelable(false);
            loadingDialog.setCanceledOnTouchOutside(false);
        }

        loadingDialog.show();
        loadingDialog.setCancelable(false);
        loadingDialog.setCanceledOnTouchOutside(false);
    }

    protected void stopLoadingDialog(AlertDialog loadingDialog) {

        if (loadingDialog == null) {
            return;
        }

        if (loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }

    protected AlertDialog getLoadingDialog(ViewGroup viewGroup) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_loading, viewGroup, false);
        builder.setView(view);

        return builder.create();
    }

    public void timerDelayRemoveDialog(long time, final Dialog d){
        Handler handler = new Handler();
        handler.postDelayed(d::dismiss, time);
    }

    protected Scheduler getProcessScheduler() {
        return Schedulers.io();
    }

    protected Scheduler getAndroidScheduler() {
        return AndroidSchedulers.mainThread();
    }

    private String getJwtToken() {
        return getSessionManager().getJwtToken();
    }

    private String getUserId() { return getSessionManager().getUserSid(); }

    // Image Picker with Esafirm
    protected void chooseImage(BottomSheetDialogFragment menu, FragmentManager fm, Fragment fragment, Integer code){
        menu.setTargetFragment(fragment, code);
        menu.show(fm, menu.getTag());
    }

    protected void closePopUpChooseImage(BottomSheetDialogFragment dialogFragment){
        dialogFragment.dismiss();
    }
}
