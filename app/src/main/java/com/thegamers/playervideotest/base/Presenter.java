package com.thegamers.playervideotest.base;

public interface Presenter<V extends BaseView> {

    void attachView(V view);

    void detachView();
}
