package com.thegamers.playervideotest.base;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.thegamers.playervideotest.R;
import com.thegamers.playervideotest.data.DataManager;
import com.thegamers.playervideotest.data.SessionManager;
import com.thegamers.playervideotest.data.api.RestService;
import com.thegamers.playervideotest.data.api.RestServiceFactory;
import com.thegamers.playervideotest.helper.Helper;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public abstract class BaseActivity extends AppCompatActivity {
    private RestService restService;
    private DataManager manager;
    private SessionManager sessionManager;


    public RestService getRestService() {
        if (restService == null) {
            restService = RestServiceFactory.create(getJwtToken(), this);
        }
        return restService;
    }


    public DataManager getManager() {
        if (manager == null) {
            manager = new DataManager(getRestService());
        }
        return manager;
    }

    public SessionManager getSessionManager() {
        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(this));
        }
        return sessionManager;
    }

    protected Scheduler getProcessScheduler() {
        return Schedulers.io();
    }

    protected Scheduler getAndroidScheduler() {
        return AndroidSchedulers.mainThread();
    }

    private String getJwtToken() {
        return getSessionManager().getJwtToken();
    }

    private String getUserId() {return getSessionManager().getUserSid();}

    protected void startLoadingDialog(AlertDialog loadingDialog) {

        if (loadingDialog == null) {
            return;
        }

        if (loadingDialog.isShowing()) {
            loadingDialog.setCancelable(false);
            loadingDialog.setCanceledOnTouchOutside(false);
        }

        loadingDialog.show();
        loadingDialog.setCancelable(false);
        loadingDialog.setCanceledOnTouchOutside(false);
    }

    protected void stopLoadingDialog(AlertDialog loadingDialog) {

        if (loadingDialog == null) {
            return;
        }

        if (loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }

    protected AlertDialog getLoadingSubmit(ViewGroup viewGroup, Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_loading_submit, viewGroup, false);
        builder.setView(view);

        return builder.create();
    }

    protected AlertDialog getLoadingDialog(ViewGroup viewGroup, Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_loading, viewGroup, false);
        builder.setView(view);

        return builder.create();
    }

    protected void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
