package com.thegamers.playervideotest.base;

public interface BaseView {

    void showError(Throwable throwable);
}
