package com.thegamers.playervideotest.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thegamers.playervideotest.R;
import com.thegamers.playervideotest.data.entity.PlaylistItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListPlayer extends RecyclerView.Adapter<AdapterListPlayer.Holder> {
    private onChoicheMovie listener;
    private onDownload download;
    private onDeleted deleted;
    private Context context;
    private List<PlaylistItem> itemList;

    public AdapterListPlayer(onChoicheMovie listener, Context context, List<PlaylistItem> itemList, onDownload download, onDeleted deleted) {
        this.listener = listener;
        this.context = context;
        this.itemList = itemList;
        this.download = download;
        this.deleted = deleted;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_movie, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        PlaylistItem player = itemList.get(i);
        holder.tvJudul.setText(player.getTitle());
        holder.tvDeskripsi.setText(player.getDescription());
        if (player.getDownload()){
            holder.btDownload.setBackgroundTintList(context.getResources().getColorStateList(R.color.green_500));
            holder.btDownload.setText("Tersimpan / Hapus");
        }else {
            holder.btDownload.setText("Simpan / Download");
            holder.btDownload.setBackgroundTintList(context.getResources().getColorStateList(R.color.blue_500));
        }
        holder.btDownload.setOnClickListener(v -> {
            if (itemList.get(i).getDownload()){
                deleted.deletedVideoDownload(itemList.get(i), i);
            }else {
                download.downloaded(itemList.get(i), i);
            }

        });

        holder.itemView.setOnClickListener(v -> {
            listener.onChoice(itemList.get(i), i);
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.bt_download)
        Button btDownload;
        @BindView(R.id.tv_deskripsi)
        TextView tvDeskripsi;
        @BindView(R.id.tv_judul_film)
        TextView tvJudul;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onChoicheMovie{
        void onChoice(PlaylistItem player, Integer position);
    }

    public interface onDownload{
        void downloaded(PlaylistItem player, Integer position);
    }

    public interface onDeleted{
        void deletedVideoDownload(PlaylistItem player, Integer position);
    }
}
