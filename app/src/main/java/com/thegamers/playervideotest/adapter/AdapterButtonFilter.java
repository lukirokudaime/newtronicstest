package com.thegamers.playervideotest.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thegamers.playervideotest.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterButtonFilter extends RecyclerView.Adapter<AdapterButtonFilter.Holder> {
    private ClickListener listener;
    private Context context;
    private List<String> itemList;
    private Integer selectedItem = 0;

    public AdapterButtonFilter(ClickListener listener, Context context, List<String> itemList) {
        this.listener = listener;
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_button, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.checkBox.setText(itemList.get(i));

        holder.checkBox.setOnClickListener(v -> {
            if (holder.checkBox.isChecked()){
                listener.onEdit(itemList.get(i));
                holder.checkBox.setChecked(true);
                Integer previousItem = selectedItem;
                selectedItem = i;
                notifyItemChanged(previousItem);
                notifyItemChanged(i);
            }else{
                holder.checkBox.setChecked(false);
                listener.onEdit("");
            }
        });

        if (selectedItem == i){
            listener.onEdit(itemList.get(i));
            holder.checkBox.setChecked(true);

        }
        if (selectedItem != i){
            if (holder.checkBox.isChecked()){
                holder.checkBox.setChecked(false);
            }

        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvCatagori)
        CheckBox checkBox;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ClickListener{
        void onEdit(String item);
    }
}
