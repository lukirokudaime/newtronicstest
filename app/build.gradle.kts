plugins {
    id("com.android.application")
}


android {
    namespace = "com.thegamers.playervideotest"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.thegamers.playervideotest"
        minSdk = 21
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"
        multiDexEnabled = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.7.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.7.0")
    implementation("androidx.navigation:navigation-fragment:2.7.6")
    implementation("androidx.navigation:navigation-ui:2.7.6")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    //screen layout
    implementation("com.intuit.ssp:ssp-android:1.0.6")
    implementation("com.intuit.sdp:sdp-android:1.0.6")
    // Network Indicator
    implementation("com.novoda:merlin:1.2.0")

    // PICASSO
    implementation("com.squareup.picasso:picasso:2.71828")

    // LOGGING
    implementation("com.jakewharton.timber:timber:4.7.1")
    implementation("androidx.multidex:multidex:2.0.1")

    // RX JAVA
    implementation("io.reactivex.rxjava2:rxjava:2.2.6")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.0") {
        exclude("io.reactivex.rxjava2", "rxjava")
    }
    // NETWORK
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("com.squareup.okhttp3:okhttp:4.8.1")
    implementation("com.squareup.okhttp3:okhttp-urlconnection:4.8.1") {
        exclude("com.squareup.okhttp3","okhttp")
    }
    implementation("com.squareup.okhttp3:logging-interceptor:4.8.1") {
        exclude ("com.squareup.okhttp3", "okhttp")
    }
    implementation("com.squareup.retrofit2:retrofit:2.9.0") {
        exclude ("com.squareup.okhttp3", "okhttp")
    }
    implementation("com.squareup.retrofit2:converter-gson:2.9.0") {
        exclude ("com.squareup.retrofit2","retrofit")
        exclude ("com.google.code.gson","gson")
    }
    implementation("com.squareup.retrofit2:adapter-rxjava2:2.9.0") {
        exclude ("com.squareup.retrofit2","retrofit")
        exclude("io.reactivex.rxjava2", "rxjava")
    }
    //exoplayter
    implementation("com.google.android.exoplayer:exoplayer:2.10.8")
    implementation("com.google.android.exoplayer:exoplayer-core:2.10.8")
    implementation("com.google.android.exoplayer:exoplayer-dash:2.10.8")
    implementation("com.google.android.exoplayer:exoplayer-hls:2.10.8")
    implementation("com.google.android.exoplayer:exoplayer-smoothstreaming:2.10.8")
    implementation("com.google.android.exoplayer:exoplayer-ui:2.10.8")
    // VIEW INJECTION
    annotationProcessor("com.jakewharton:butterknife-compiler:10.2.3")
    implementation("com.jakewharton:butterknife:10.2.3") {
        exclude ("com.androidx.support", "support-annotations")
        exclude ("com.androidx.support","support-compact")
    }
}